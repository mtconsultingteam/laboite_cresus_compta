##############################################################################

# -*- coding: utf-8 -*-

import base64
import cStringIO

from openerp import tools
from openerp.osv import fields,osv
from openerp.tools.translate import _
from openerp.tools.misc import get_iso_codes





class fichier_compta(osv.osv_memory):
    _name = "fichier.compta"
    _columns = {
        'date_debut': fields.date('Date debut'),
        'date_fin': fields.date('Datedate_debut'),
        'data': fields.binary('File', readonly=True),
        'name': fields.char('File Name', readonly=True),
        'comprobante_fname': fields.char('Comp', size=32, readonly=True),
#         'comprobante': fields.binary(string='Comprobante'),

    }
    
    _defaults = {
    'comprobante_fname': 'export.ecc',
    }
    
    def account_move_line_export(self, cr, uid, ids, context=None):
        this = self.browse(cr, uid, ids)[0]
        cr.execute('''select  aml.date date,aml.debit debit ,aml.credit credit, am.name pce ,
        aml.name libelle , aml.id idno , aml.account_tax_id codetva, aml.analytic_account_id codeanalyt
            from account_move_line aml 
            
            LEFT JOIN  account_move am  ON aml.move_id = am.id
             ''')
        res = cr.fetchall()
        
        fo = open("export.ecc", "w+")
        account_move_line_obj = self.pool.get('account.move.line')
        res_currency_rate_obj = self.pool.get('res.currency.rate')

# Close opend file

        for line in res:
            account_move_line_brw = account_move_line_obj.browse(cr, uid, line[5], context=context)
            currency_id = account_move_line_brw.invoice.currency_id.id
            montant = account_move_line_brw.move_id.amount
            res_currency_rate_obj_id = res_currency_rate_obj.search(cr, uid, [('currency_id', '=',currency_id )], context=context)
            cours = 1
            if res_currency_rate_obj_id and res_currency_rate_obj.browse(cr, uid, res_currency_rate_obj_id[0], context=context).rate:
                cours = res_currency_rate_obj.browse(cr, uid, res_currency_rate_obj_id[0], context=context).rate
            montant_me = montant * cours
            
            date = str(line[0])
            debit = str(line[1])
            credit = str(line[2])
            pce = str(line[3])
            libelle = str(line[4])
            idno  = str(line[5])
            codetva = str(line[6])  
            codeanalyt = str(line[7])
            

            fo.write( date + "\t" + debit  + "\t" + credit + "\t" +  pce  + "\t" + libelle + "\t" + 
                      str(montant) + "\t" + str(montant_me) + "\t" + str(cours) + "\t" + 
                      "0\t" + "0\t" + "0\t" + "0\t" + 
                      idno  + "\t" + codetva + "\t" +  codeanalyt + "\n" );
        fo.seek(0)
        data = base64.encodestring(fo.read())
        fo.close()
        this.name = 'export.ecc'
        self.write(cr, uid, ids, { 
                                  'data': data,
                                  'name':this.name }, context=context)     
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'fichier.compta',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': this.id,
            'views': [(False, 'form')],
            'target': 'new',
        }

fichier_compta()

