# -*- coding: utf-8 -*-

{'name': 'laboite_cresus_compta',
 'summary': '',
 'description': """""",
 'version': '1.1',
 'author': 'mtconsulting',
 'category': 'account',
 'website': 'http://mtconsulting.laboite-sarl.ch',
 'depends': ['account'],
 'data': ["fichier_compta.xml"],
 'demo': [],
 'test': [],
 'auto_install': False,
 'installable': True,
 'images': []
 }
